import pygame
from time import sleep
from random import randint
from pygame.locals import *
from buttonDef import *
from highscoresInGame import *
red=(255,0,0)
green=(0,255,0)
blue=(0,0,255)
pygame.init()
boxCount=0
score=0
screen=pygame.display.set_mode((640,480))
def game():
    boxCount=0
    score=0
    class plane: #### classes: make one, can use as many as you want, ln 85
        def __init__(self,X,Y): ####with functions inside classes you can call on one or all depending on how the code is
            self.plane=pygame.image.load("fighterplane.png")####loads image
            self.X=X
            self.Y=Y
            self.b=[]####Fixes the Index out of Range Error, list for bullets
            self.score=0
        def plane_refresh(self,boxList):
            screen.blit(self.plane,(self.X,self.Y))
            for i in range(len(self.b)-1):####Fixes the Index out of Range Error in Shoot
                if self.b[i].X>639:
                    self.b.remove(self.b[i])####physical limit for bullets
                self.b[i].bullet_refresh(boxList)####gives code from boxes to bullets, ln 37
            for i in boxList:####runs through list of boxes, checks collision physics
                for j in self.b:
                    if i.X<=j.X<=i.X+32 and i.Y<=j.Y<=i.Y+32:
                        i.box_explode()
                        self.b.remove(j)
                        self.score=self.score+1
                if (self.X<=i.X<=self.X+55 and self.Y-7<=i.Y<=self.Y+40) or (self.X+30<=i.X<=self.X+41 and self.Y-30<=i.Y<=self.Y+68):
                    getScore(self.score)
        def plane_shoot(self):
            self.b.append(bullet(self.X+68,self.Y+28))####spawns bullet at tip of plane bc adds one onto the list
            for i in self.b:
                i.bullet_shoot()####for every bullet in list, it is shot, ln 45
        def plane_up(self):
            self.Y=self.Y-10
        def plane_down(self):
            self.Y=self.Y+10
        def plane_right(self):
            self.X=self.X+10
        def plane_left(self):
            self.X=self.X-10
            
        
    class bullet:
        def __init__(self,X,Y):
            self.bullet=pygame.transform.scale(pygame.image.load("bullet.png"),(10,10))
            self.X=X
            self.Y=Y
            self.state=0
        def bullet_refresh(self,boxList):
            screen.blit(self.bullet,(self.X,self.Y))
            if self.state==1:
                self.X=self.X+10
        def bullet_shoot(self):
            self.state=1

    class box:
        def __init__(self,X,Y):
            self.box=pygame.image.load("box1.gif")
            self.expFrm=[]
            for i in range (0,3,1):
                self.expFrm.append(pygame.image.load("exp"+str(i)+".png"))####loads in separate boxes
            self.X=X
            self.Y=Y
            self.state=0
            self.explode=0
            self.hitX=0
        def box_spawn(self):
            self.state=1
        def box_explode(self):
            self.explode=1
            self.hitX=self.X
        def box_refresh(self):
            if self.state==1:
                screen.blit(self.box,(self.X,self.Y))
                self.X=self.X-10
            if self.X<1:
                self.X=randint(600,630)####for if box goes offscreen
                self.Y=randint(20,460)
                self.state=1
            if self.explode==1:
                if self.hitX-self.X==10:
                    screen.blit(self.expFrm[0],(self.X+15,self.Y+6))
                if self.hitX-self.X==20:
                    screen.blit(self.expFrm[1],(self.X+10,self.Y+1))
                if self.hitX-self.X==30:####animation code, relies on position
                    screen.blit(self.expFrm[2],(self.X+10,self.Y+1))
                if self.hitX-self.X==40:
                    self.explode=0
                    self.state=0
                    while self.X>0:
                        self.X=self.X-10
                    
    screen=pygame.display.set_mode((640,480))
    p1=plane(200,200)
    boxList=[]
    for i in range (1,3,1):
        boxList.append(box(620,randint(20,460)))####adding boxes to list
    for i in boxList:####spawning boxes in list
        i.box_spawn()
    clock=pygame.time.Clock()
    while True:
        screen.fill((255,255,255))
        p1.plane_refresh(boxList)####gives code from boxes to plane
        for i in boxList:
            i.box_refresh()
        for event in pygame.event.get():
            if event.type==QUIT:
                pygame.quit()
                exit()
        if event.type==KEYDOWN:
            if event.key==K_z:
                p1.plane_shoot()
            elif event.key==K_UP:
                p1.plane_up()
            elif event.key==K_DOWN:
                p1.plane_down() ####code for movement of plane, ln 24
            elif event.key==K_LEFT:
                p1.plane_left()
            elif event.key==K_RIGHT:
                p1.plane_right()
        boxCount=boxCount+1
        if boxCount>=150:
            boxList.append(box(620,randint(20,460)))
            boxCount=0
            for i in boxList:
                i.box_spawn()
        show_text(screen,"Score: "+str(p1.score),(0,0,0),40,20)
        clock.tick(17)
        pygame.display.update()
while True:
    screen.fill((0,0,0,))
    show_text(screen,'BOX DEFENSE',(255,255,255),320,100)
    button(screen,"Play",red,green,100,240,100,50,game)
    button(screen,"Quit",green,red,440,240,100,50,exit)
    for event in pygame.event.get():
        if event.type==QUIT:
            pygame.quit()
            exit()

    pygame.display.update()

import pygame
from pygame.locals import *
pygame.init()
def show_text(screen,msg,color,x,y):
    fontobj=pygame.font.SysFont('comicsans',20)
    msobj=fontobj.render(msg,False,color)
    a,b,w,h=msobj.get_rect()
    screen.blit(msobj,(x-w/2,y-h/2))
def button(screen,msg,ic,ac,x,y,w,h,action=None):
    mouse=pygame.mouse.get_pos()
    click=pygame.mouse.get_pressed()
    if x+w>mouse[0]>x and y+h>mouse[1]>y:
        pygame.draw.rect(screen,ac,(x,y,w,h))
        if click[0] == 1 and action!=None:
            action()
    else:
        pygame.draw.rect(screen,ic,(x,y,w,h))
    show_text(screen,msg,(0,0,0),x+w/2,y+h/2)
